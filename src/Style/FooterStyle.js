import styled from "styled-components";

export const TitleFooter = styled.h1`
  margin-top: 50px;
  color: white;
  letter-spacing: 1px;
  font-weight: bold;
  font-size: 30px;
  @media (min-width: 600px) and (max-width: 1000px) {
      font-size: 20px !important;
  }
`;

export const TextFooter = styled.p`
  margin: 0;
  text-align: justify;
  color: white;
  line-height: 1.6;
  padding-right: 40px;
  opacity: 0.7;
  font-size: 14px;
  @media (min-width: 600px) and (max-width: 1000px) {
      font-size: 12px;
  }
`;

export const BtnGoTop = styled.a`
  position: fixed;
  bottom: 30px;
  right: 30px;
  padding: 10px 15px;
  border: 2px solid rgb(64, 225, 172);
  background-color: white;
  color: rgb(64, 225, 172);
  opacity: 0.6;
  -webkit-transition: all 0.4s linear;
  transition: all 0.4s linear;
  cursor: pointer;
  border-radius: 50%;

  :hover {
    color: white;
    background-color: rgb(64, 225, 172);
  }
`;

export const Icon = styled.i`
  color: white;
  font-size: 30px;
  padding-right: 16px;
  cursor: pointer;
  -webkit-transition: all 0.3s linear;
  &:hover {
    transition: all 0.3s linear;
    transform: scale(1.1);
    color: rgb(64, 225, 172);
  }
`;

export const WapperIcons = styled.div`
  text-align: center;
  padding-top: 20px;
`;

import styled from "styled-components";

export const BtnLang = styled.button`
    background-color: white;
    margin-top: 20px;
    text-transform: uppercase;
    color: rgb(64, 225, 172);
    width: 50px;
    height: 30px;
    border: 2px solid rgb(64, 225, 172);
    padding: 4px 12px;
    font-size: 14px;
    background-color: ${(props) =>
    props.bg == "add" ? "rgb(64, 225, 172)" : null};
    color: ${(props) => (props.bg == "add" ? "white" : null)};
`;

export const BtnFunc = styled.a`
  color: rgb(64, 225, 172);
  background-color: white;
  border: 2px solid rgb(64, 225, 172);
  padding: 10px 20px;
  font-size: 20px;
  text-decoration: none;

  &:hover{
    background-color: rgb(64, 225, 172);
    color: white;
    transition: all .4s linear;
  }
`;

export const BtnFind = styled.button`
    padding: 10px 16px;
    font-size: 14px;
    border-radius: 4px;
    border: 2px solid rgb(64, 225, 172);
    background-color: transparent;
    color: white;
    cursor: pointer;
    transition: all .4s linear;
    width: 100px;
    height: 40px;
    display: inline-flex;
    text-align: center;
    justify-content: space-around;
    align-items: center;
    width: 100%;
    &:hover .fa-arrow-right-long{
        margin-left: 10px;
        transition: all .4s linear;
    }
    &:hover{
        background-color: white;
        transition: all .4s linear;
        color: rgb(64, 225, 172);
    }
`;


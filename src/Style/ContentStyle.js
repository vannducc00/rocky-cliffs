import styled from "styled-components";

export const TitleContent = styled.h1`
  font-size: 50px;
  text-align: center;
  padding-bottom: 40px;
  font-weight: bold;
  margin-top: 90px;
  padding-bottom: 60px;
`;

export const ImageAbout = styled.img`
  width: 70%;
  @media (min-width: 600px) and (max-width: 1000px) {
    width: 80%;
  }
`;

export const TextAbout = styled.p`
  letter-spacing: 1px;
  padding-top: 20px;
  color: gray;
  line-height: 1.5;
  text-align: justify;
  @media (min-width: 600px) and (max-width: 1000px) {
    font-size: 13px;
    text-align: justify;
    width: 80%;
  }
`;

export const Question = styled.h3`
  font-size: 30px;
  color: rgb(64, 225, 172);
  font-weight: bold;
`;

export const WapperBlock = styled.div`
  position: relative;
  margin-top: 40px;
  width: 350px;
  padding-left: 30px;
`;

export const Line = styled.div`
  &::before {
    content: "";
    width: 100px;
    border-top: 4px solid rgb(64, 225, 172);
    position: absolute;
    top: 16px;
    left: -90px;
  }
`;

export const Line1 = styled.div`
  &::before {
    content: "";
    width: 100px;
    border-top: 4px solid rgb(64, 225, 172);
    position: absolute;
    top: 60px;
    left: -47px;
  }
`;

export const WapperAbout = styled.div`
  margin-top: 30px;
`;
export const WapperImage = styled.div`
  text-align: ${(props) => (props.textAlign ? "right" : null)};
  position: relative;
`;

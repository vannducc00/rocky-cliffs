import styled from "styled-components";

export const TitleBanner = styled.h1`  
    color: white;
    font-size: 55px;
    font-weight: bold;
`;

export const TextFocus = styled.p`
    color: rgb(64, 225, 172);
    position: absolute;
    bottom: 0;
    left: 10px;
    z-index: -1;
    transition: all 0.4s linear;

    bottom: ${props=>props.fly?"70px !important":"null"};  
    left:${props=>props.fly?"0":"null"};
    transition:${props=>props.fly?"all 0.4s linear":"null"};
`;

export const Intro1 = styled.p`
    color: rgb(199, 198, 198);
    letter-spacing: 2px;
    display: block;
    margin-bottom: 6px;
`;

export const Intro2 = styled.p`
    color:rgb(64, 225, 172);
    font-size: 16px;
    display: block;
    margin-top: 20px;
    letter-spacing: 2px;
    position: relative;
  margin-bottom: ${props=>props.marginBottom?"50px":null};

`;

export const ImgBanner = styled.img`
   width: 100%;
    height: 550px;
    object-fit: cover;
    filter: brightness(0.5);
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: -1;
`;

export const InputBanner = styled.input`
     border: 2px solid rgb(64, 225, 172) !important;
    padding: 14px 50px 14px 16px;
    font-size: 16px;
    outline: none;
    border-radius: 4px;
    width: ${props=>props.results?"100%":null};
    height:${props=>props.results?"40px":null};
    color: ${props=>props.results?"white":null};
`;

export const Origin = styled.p`
      font-family: "Freestyle Script";
    font-size: 30px;
    text-shadow: 2px 2px 5px rgb(64, 225, 172);
    text-align: right;
    color: white;
    padding-top: 100px;
`;


export const WapperRight = styled.div`
     margin-top: 100px;
`;


import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import ButtonLang from "./ButtonLang";
import { BtnFunc, BtnFind } from "../Style/Button";
import {
  Intro1,
  TitleBanner,
  TextFocus,
  Intro2,
  ImgBanner,
  InputBanner,
  Origin,
  WapperRight,
} from "../Style/HeaderContent";

export default function Header() {
  const axios = require("axios").default;
  const { t, i18n } = useTranslation();
  const [isFocus, setisFocus] = useState(false);
  const [isVal, setisVal] = useState("");
  const [result, setresult] = useState("The result will be displayed here!");
  const [isLoading, setisLoading] = useState(false);

  const hadlePlacehoder = () => {
    setisFocus(true);
  };

  const hadlePlacehoderOut = () => {
    setisFocus(false);
  };

  const handleSearch = () => {
    if (isVal == "" || isVal == null) {
      alert(t("you need to enter Facebook link"));
    } else {
      setisLoading(true);
      axios
        .get("https://dc.winds.vn/users?id=" + isVal)
        .then(function (res) {
          setisLoading(false);
          if (Object.keys(res.data.data).length === 0) {
            setisLoading(false);
            setresult("Cannot find the phone number!");
          } else {
            let data = res.data.data.phone;
            toString(data);
            let cv = data.slice(0, 2) + " " + data.slice(2);
            let convert = `+${cv}`;
            setresult(convert);
          }
        })
        .catch(function (err) {
          alert(t("System error!!!"));
          setisLoading(false);
        });
    }
  };

  return (
    <div className="Header">
      <ImgBanner
        className="banner"
        src="https://images.unsplash.com/photo-1562157697-74f3f1deab4f?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=60&raw_url=true&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTM1fHxhZXN0aGV0aWN8ZW58MHx8MHx8&auto=format&fit=crop&w=600"
      />
      <div className="row">
        <div className="col-sm-6 col-md-7 col-lg-7 col-xl-7">
          <div className="left-content">
            <Intro1 className="intro1">{t("Hi guys!")}</Intro1>
            <TitleBanner>
              {t("Find the phone number of your crush now!")}
            </TitleBanner>
            <Intro2 marginBottom className="intro2">{t("A mint of creativity")}</Intro2>
            <div className="contai-btn hide">
              <BtnFunc href="#about" className="btn-func">
                {t("Instructions")}
              </BtnFunc>
              <BtnFunc
                href="#contact"
                className="btn-func"
                style={{ marginLeft: "20px" }}
              >
                {t("Contact us")}
              </BtnFunc>
            </div>
          </div>
        </div>
        <div className="col-sm-6 col-md-5 col-lg-5 col-xl-5">
          <WapperRight className="right-content">
            <div className="input-value">
              <div style={{ position: "relative" }}>
                <TextFocus
                  fly={isFocus}
                  className={isFocus ? "text-focus fly" : "text-focus"}
                >
                  Facebook link...
                </TextFocus>
                <InputBanner
                  className="border"
                  style={{ width: "100%", marginBottom: "20px" }}
                  type="text"
                  placeholder={!isFocus ? "Facebook link..." : null}
                  onFocus={hadlePlacehoder}
                  onBlur={hadlePlacehoderOut}
                  onChange={(e) => setisVal(e.target.value)}
                />
              </div>
            </div>
            <div className="row">
              <div className="col-4 col-sm-4 col-md-4 col-lg-3">
                {!isLoading ? (
                  <BtnFind className="btn-find" onClick={handleSearch}>
                    {t("Find")} <i className="fa-solid fa-arrow-right-long"></i>
                  </BtnFind>
                ) : (
                  <BtnFind
                    className="btn-find"
                    style={
                      isLoading ? { cursor: "wait" } : { cursor: "pointer" }
                    }
                    disabled={isLoading ? true : false}
                    onClick={handleSearch}
                  >
                    {t("Finding")}
                  </BtnFind>
                )}
              </div>

              <div className="col-8 col-sm-8 col-md-8 col-lg-9">
                <InputBanner
                  results
                  className="border result"
                  type="text"
                  disabled
                  value={t(result)}
                  style={
                    result != "Cannot find the phone number!"
                      ? null
                      : { color: "red" }
                  }
                />
              </div>
            </div>

            <Origin className="origin">A Product Of WindSoft</Origin>
          </WapperRight>
        </div>
      </div>
    </div>
  );
}

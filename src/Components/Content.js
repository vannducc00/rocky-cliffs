import React from "react";
import { useTranslation } from "react-i18next";
import {TitleContent,
  ImageAbout,
  TextAbout,
  Question,WapperBlock,Line,Line1,WapperAbout,WapperImage } from "../Style/ContentStyle"

export default function Content() {
  const { t, i18n } = useTranslation();
  return (
    <div>
      <WapperAbout className="About_1">
        <TitleContent className="title">{t("About this tool")}</TitleContent>
        <div className="row">
          <div className="hide col-md-6">
            <WapperImage textAlign className="con-image">
              <ImageAbout
                className="image-about hide"
                src="https://images.unsplash.com/photo-1595284843439-f963be7ed526?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=60&raw_url=true&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTB8fGdyZWVuJTIwcGhvbmV8ZW58MHx8MHx8&auto=format&fit=crop&w=600"
                alt=""
              />
            </WapperImage>
          </div>
          <div className="col-12 col-md-6">
            <WapperBlock className="con-note">
            <Line className="line hide"></Line>
              <Question className="text-tittle">{t("What is this tool?")}</Question>
              <TextAbout className="text-content">
                {t(
                  "By a few simple actions, you can easily find out the phone number of anyone!"
                )}
              </TextAbout>
            </WapperBlock>
          </div>
        </div>
      </WapperAbout>
      <WapperAbout textAlign className="About_1">
        <div className="row">
          <div className="col-md-6">
            <WapperBlock className="con-note respon-about" style={{float:"right",paddingRight:"40px", textAlign:"right"}}>
              <Question className="text-tittle">{t("How to use?")}</Question>
              <TextAbout className="text-content">
                {t(
                  "You just need to paste the Facebook URL into the input field and then click the button. We will take care of the rest of work!"
                )}
              </TextAbout>
            </WapperBlock>
          </div>
          <div className="col-md-6">
            <WapperImage className="con-image_1">
          <Line1 className="line_1 hide"></Line1>
              <ImageAbout
                className="image-about hide"
                src="https://images.unsplash.com/photo-1511707171634-5f897ff02aa9?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=60&raw_url=true&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8cGhvbmV8ZW58MHx8MHx8&auto=format&fit=crop&w=600"
                alt=""
              />
            </WapperImage>
          </div>
        </div>
      </WapperAbout>
      <WapperAbout className="About_1">
        <div className="row">
          <div className="col-md-6">
            <WapperImage textAlign className="con-image">
              <ImageAbout
                className="image-about hide"
                src="https://images.unsplash.com/photo-1509395062183-67c5ad6faff9?crop=entropy&cs=tinysrgb&fm=jpg&ixlib=rb-1.2.1&q=60&raw_url=true&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NjV8fHBob25lfGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=600"
                alt=""
              />
            </WapperImage>
          </div>
          <div className="col-md-6">
            <WapperBlock className="con-note">
            <Line className="line hide"></Line>
              <Question className="text-tittle">{t("Why should choose us?")}</Question>
              <TextAbout className="text-content">
                {t(
                  "By a few simple actions, you can easily find out the phone number of anyone!"
                )}
              </TextAbout>
            </WapperBlock>
          </div>
        </div>
      </WapperAbout>
    </div>
  );
}

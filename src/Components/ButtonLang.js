import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import {BtnLang} from "../Style/Button";

export default function ButtonLang() {
  const { t, i18n } = useTranslation();
  const [toggle, settoggle] = useState("vn");

  const changeLanguageHandler = (e) => {
    const lang = e.target.name;
    i18n.changeLanguage(lang);
    settoggle(lang);
  };

  return (
    <div>
      <BtnLang
        className="btn-lang btn-en"
        bg={toggle == "en" ? "add" : null}
        name="en"
        onClick={(e) => changeLanguageHandler(e)}
      >
        en
      </BtnLang>
      <BtnLang
        className="btn-lang btn-vn"
        bg={toggle == "vn" ? "add" : null}
        name="vn"
        onClick={(e) => changeLanguageHandler(e)}
      >
        vn
      </BtnLang>
    </div>
  );
}

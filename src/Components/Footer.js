import React from "react";
import { useTranslation } from "react-i18next";
import {
  TextFooter,
  TitleFooter,
  Icon,
  WapperIcons,
} from "../Style/FooterStyle";

export default function Footer() {
  const { t, i18n } = useTranslation();
  const dataIcons = [
    {
      icon: "fa-facebook",
    },
    {
      icon: "fa-linkedin",
    },
    {
      icon: "fa-instagram",
    },
    {
      icon: "fa-twitter",
    },
  ];

  return (
    <div style={{ padding: "20px 0", marginTop: "40px" }}>
      <div
        className="row"
        style={{ borderBottom: "1px solid gray", paddingBottom: "30px" }}
      >
        <div className="col-12 col-sm-6 col-md-6 col-lg-8">
          <div className="padding-left about-us">
            <TitleFooter className="title-footer">{t("About us")}</TitleFooter>
            <TextFooter className="text-footer">
              {t(
                "As an application and software development company that helps businesses raise brands, improve business efficiency, boost sales and accelerate business digital transformation. WINDSoft focuses on improving sales quality. customers, focusing on the connection between customers and businesses, optimizing processes and automating work."
              )}
            </TextFooter>
          </div>
        </div>
        <div className="col-6 col-sm-3 col-md-3 col-lg-2">
          <div className="padding-left services">
            <TitleFooter className="title-footer">{t("Services")}</TitleFooter>
            <TextFooter className="text-footer">{t("App Mobile")}</TextFooter>
            <TextFooter className="text-footer">{t("Shop Cloud")}</TextFooter>
            <TextFooter className="text-footer">{t("EZSale")}</TextFooter>
          </div>
        </div>
        <div className="col-6 col-sm-3 col-md-3 col-lg-2">
          <div className="padding-left contacts">
            <TitleFooter className="title-footer">{t("Contacts")}</TitleFooter>
            <TextFooter className="text-footer">
              {t("Email: admin@winds.vn")}
            </TextFooter>
            <TextFooter className="text-footer">
              {t("Phone: 0987075454")}
            </TextFooter>
            <TextFooter className="text-footer">{t("Website:")}</TextFooter>
            <TextFooter className="text-footer">https://winds.vn/</TextFooter>
          </div>
        </div>
      </div>
      <WapperIcons className="link">
        {dataIcons.map((item) => (
          <Icon className={"fa-brands " + item.icon}></Icon>
        ))}
      </WapperIcons>
    </div>
  );
}

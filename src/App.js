import "./App.css";
import { useTranslation } from "react-i18next";
import ButtonLang from "./Components/ButtonLang";
import Header from "./Components/Header";
import Content from "./Components/Content";
import Footer from "./Components/Footer";
import "./Style/Style.css";
import { useEffect, useState } from "react";
import { BtnGoTop } from "./Style/FooterStyle"

function App() {
  const { t, i18n } = useTranslation();
  const [isScroll, setisScroll] = useState(false)

 useEffect(() => {
   const handleUp=()=>{
     if(window.scrollY >=200){
      setisScroll(true)
     }else{
      setisScroll(false)
     }
   }

   window.addEventListener("scroll",handleUp)
 }, [])
 

  return (
    <div className="">
      <div className="container">
        <div className="respon" style={{ height: "550px" }}>
          <div id='top' className="zone-lang">
            <ButtonLang />
          </div>
          <div className="">
            <Header />
          </div>
        </div>
        <div id="about" className="content">
          <Content />
        </div>
      </div>
      <div style={{ backgroundColor: "black" }}>
        <div id='contact' className="container">
          <Footer />
        </div>
      </div>
     {isScroll && <BtnGoTop href='#top' className="btn-up">
        <i className="fa-solid fa-arrow-up"></i>
      </BtnGoTop>}

    </div>
  );
}

export default App;
